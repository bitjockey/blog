---
layout: post
title:  "Introduction"
date:   2020-01-03 15:55:21 -0600
categories: blog
---

I suppose it was bound to happen sooner or later, that I'd jump into the one topic that this industry is buzzing about (for good reason). I've been more or less on a path that has lead to this, and now I'm trying to document what I've learned.

In the past few months, I've learned about some of the basic machine learning algorithms from an introductory course (e.g. logistic regression, neural networks, support vector machines, and so on). I didn't really have much confidence at first that I could get through all that, but somehow I did.

I'm now taking another course to learn how to take what I've learned and apply it to several problems. It's not enough to just read about and/or listen to algorithms, you need to apply them to really solidify that understanding. So that's what I've been focusing on lately (while finishing up the other course).
