---
layout: page
title: "About"
permalink: /about/
---

## What does your name mean?

> **bit jockey** _a programmer_

It's a slang term for a programmer from the cyberpunk/urban fantasy tabletop rpg Shadowrun.

## Who are you?

I am a software engineer who is currently working on machine learning applications and occasionally, robotics.

## What is this about?

This is meant to be a blog chronicle my adventures in this field.
